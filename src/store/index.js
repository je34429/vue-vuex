import Vuex from 'vuex'

const store = new Vuex.Store({
  state: {
    titleApp:"Memes",
    memes:[]
  },
  mutations: {
    /*actualiza estados*/
    setMemes(state, payload){
      state.memes = payload;
    }
  },
  actions:{
    /*ejecuta lógica asíncrona de peticiones para luego ejecutar un mutation*/
    async getMemes({commit}, params){
      try {
        const response = await fetch("https://api.imgflip.com/get_memes");
        const result = await response.json();
        

        if(!params?.total || params.total == "All" ){
          store.commit("setMemes");
        }else{
          const resultTemp = [];
          console.log("despachando "+ params.total+' memes');
          result.data.memes.forEach((meme,index) => {
            if(index < params.total) resultTemp.push(meme);
          });
          commit("setMemes", resultTemp);
        }
        //ejecutamos la mutation
        
        
        console.log(result);
      } catch (error) {
        console.log(error);
      }
    }
  },
  modules: {
  }
});

export default store;
